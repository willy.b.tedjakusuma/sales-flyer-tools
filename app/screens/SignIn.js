import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Button from "../components/Button/Button";
import colors from '../config/colors';
import InputText from '../components/InputText/InputText';
import TitleText from '../components/TitleText/TitleText';

export default class SignIn extends Component {
  render() {
    return (
      <View style={styles.background}>
        <TitleText fontSize={50}/>
        <InputText placeholder='Username'/>
        <InputText placeholder='Password'/>
        <Button buttonText='SIGN IN' onPress={() => this.props.navigation.navigate('Home')}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.primary
  }
});



import React, { Component } from 'react';
import { View } from 'react-native'
import Navigator from './../config/homeNavigation';
import HeaderContainer from '../components/HeaderContainer/HeaderContainer'
export default class Home extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderContainer/>
        <Navigator/>
      </View>
    )
  }
}

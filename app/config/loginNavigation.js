import { createStackNavigator, createAppContainer } from 'react-navigation';
import SigninScreen from '../screens/SignIn';
import HomeScreen from '../screens/Home'
const Navigator = createStackNavigator({
  Signin: SigninScreen ,
  Home : HomeScreen
}, {
  defaultNavigationOptions: {
    header: null
  }
})

export default createAppContainer(Navigator)

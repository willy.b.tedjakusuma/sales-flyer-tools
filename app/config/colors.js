export default {
  primary: '#2A4D69',
  secondary: '#4B86B4',
  light: '#FFFFFF',
  dark: '#000000'
}
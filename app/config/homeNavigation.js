import React from 'react' // For Icon in tabBarIcon
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import LeadScreen from '../screens/Leads';
import FlyerScreen from '../screens/Flyers';
import colors from './colors'
import { Icon } from 'react-native-elements';

const Navigator = createBottomTabNavigator({
  Flyers: FlyerScreen,
  Lead: LeadScreen
}, 
{
  defaultNavigationOptions: ({navigation}) => ({
    tabBarIcon: ({focused, tintColor}) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Lead') {
        iconName = "account-group" + (focused ? '' : '-outline')
      } else if (routeName === 'Flyers') {
        iconName = 'file-document-box-multiple' + (focused ? '' : '-outline')
      }
      
      return <Icon name={iconName} type='material-community' size={36} color={tintColor}/>
    },
    tabBarOptions : {
      style: {
        backgroundColor: colors.primary,
      },
      showLabel: false,
      activeTintColor: colors.light,
      inactiveTintColor: colors.secondary,
    }
  })
})
  // {

export default createAppContainer(Navigator)

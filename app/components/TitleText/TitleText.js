import React, { Component } from 'react'
import { Text, View } from 'react-native'
import styles from './styles';

export default class TitleText extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
        <Text style={[styles.title, {fontSize: this.props.fontSize, margin: this.props.margin} ]}> Go Publish </Text>
      </View>
    )
  }
}

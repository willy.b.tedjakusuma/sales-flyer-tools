import { StyleSheet } from 'react-native';
import colors from '../../config/colors';

export default StyleSheet.create({
  title: {
    color: colors.light,
    fontFamily: 'GrandHotel',
    alignSelf: 'center',
    marginBottom: 40
  }
});


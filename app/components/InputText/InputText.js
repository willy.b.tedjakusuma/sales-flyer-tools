import React, { Component } from 'react'
import { TextInput, View } from 'react-native'
import styles from './styles'
import { Icon } from 'react-native-elements'

export default class InputText extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let iconName = this.props.placeholder.toUpperCase() == 'USERNAME' ? 'person-outline' : 'https'
    return (
      <View style={styles.container}>
        <Icon name={iconName} type='material' iconStyle={styles.icon}/>
        <TextInput placeholder={this.props.placeholder} />
      </View>
    )
  }
}

import { StyleSheet } from 'react-native';
import colors from '../../config/colors'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: colors.light,
    borderRadius: 50,
    width: 300,
    margin: 5,
    alignSelf: 'center'
  },
  icon: {
    margin: 12,
    color: colors.primary
  }
  
})
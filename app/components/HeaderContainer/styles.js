import { StyleSheet } from 'react-native'
import colors from '../../config/colors'
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: colors.primary
  }
})

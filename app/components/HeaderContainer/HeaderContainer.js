import React, { Component } from 'react'
import { View } from 'react-native'
import TitleText from '../TitleText/TitleText'
import { Icon } from 'react-native-elements'
import styles from './styles'
export default class HeaderContainer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Icon name='git' type='material-community' iconStyle={{margin: 5, color: 'white'}}/>
        <TitleText fontSize={20} margin={5}/>
      </View>
    )
  }
}

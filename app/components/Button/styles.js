import { StyleSheet } from 'react-native';
import colors from '../../config/colors';

export default StyleSheet.create({
  button: {
    alignSelf: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 50,
    backgroundColor: colors.secondary,
    borderColor: '#8492A6',
    borderWidth: 0.5,
    width: 300,
    margin: 20
  },
  text: {
    color: colors.light,
    fontWeight: 'bold',
    alignItems: 'center'
  }
});
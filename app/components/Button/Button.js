import React, { Component } from 'react'
import { TouchableHighlight, Text} from 'react-native'

import styles from '../Button/styles'

export default class Button extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return(
      <TouchableHighlight style={styles.button} onPress={this.props.onPress}>
        <Text style={styles.text}>{this.props.buttonText}</Text>
      </TouchableHighlight>
    )
  }
}
